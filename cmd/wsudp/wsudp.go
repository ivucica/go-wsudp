package main // badc0de.net/pkg/wsudp/cmd/wsudp

import (
	"flag"
	"net"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/golang/glog"
	"golang.org/x/net/websocket"
)

var (
	quit        chan struct{}
	udpAddr     = flag.String("udp_addr", ":9691", "listening address for udp packets; also the source for responses")
	udpDest     = flag.String("udp_dest", "255.255.255.255:9691", "destination for responses")
	webAddr     = flag.String("web_addr", ":9691", "listening address for web")
	webHtmlRoot = flag.String("web_html_root", os.Getenv("GOPATH")+"/src/badc0de.net/pkg/wsudp/html_root", "location of pages to be served under /web/")
	webHostname = flag.String("web_hostname", mustString(os.Hostname()), "hostname under which the web pages will be served")
	webOrigin   = flag.String("web_origin", "http://"+mustString(os.Hostname()), "origin for use by websockets")

	udpSync     = sync.Mutex{}
	udpConn     *net.UDPConn
	udpDestAddr *net.UDPAddr

	webSocketsSync = sync.Mutex{}
	webSockets     = make(map[*websocket.Conn]chan struct{})
)

func mustString(s string, err error) string {
	if err != nil {
		panic(err)
	}
	return s
}

func udp() {
	defer func() {
		quit <- struct{}{}
	}()

	l, err := net.ListenPacket("udp", *udpAddr)
	if err != nil {
		glog.Errorf("error listening for udp packets: %s", err)
		return
	}
	glog.Infof("listening on udp address %s", *udpAddr)
	udpConn = l.(*net.UDPConn)
	for {
		b := make([]byte, 1500)
		count, addr, err := udpConn.ReadFromUDP(b)
		if err != nil {
			glog.Errorf("quitting due to error reading udp packet: %s", err)
			return
		}
		glog.Infof("read %d bytes from %s", count, addr)
		broadcast(b[:count])

		// n.b. messages we sent out ourselves are not ignored.

	}
}

func broadcast(b []byte) {
	webSocketsSync.Lock()
	defer webSocketsSync.Unlock()

	for ws, c := range webSockets {
		glog.Infof("sending to %+v", ws.Config())
		_, err := ws.Write(b)
		if err != nil {
			close(c)
			delete(webSockets, ws)
		}
	}
}

func homepage(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/web/", http.StatusFound)
}

func web() {
	glog.Infof("about to listen on web addr %s", *webAddr)

	http.HandleFunc("/", homepage)
	http.Handle("/web/", http.StripPrefix("/web", http.FileServer(http.Dir(*webHtmlRoot))))
	http.Handle("/ws", websocket.Handler(ws))

	err := http.ListenAndServe(*webAddr, nil)
	if err != nil {
		glog.Errorf("error serving web on %s: %s", *webAddr, err)
	}
	<-quit
}

func ws(ws *websocket.Conn) {
	glog.Infof("new websocket connection: %+v", *ws.Config())
	webSocketsSync.Lock()
	c := make(chan struct{})
	webSockets[ws] = c
	webSocketsSync.Unlock()

	defer func() {
		webSocketsSync.Lock()
		close(c)
		delete(webSockets, ws)
		webSocketsSync.Unlock()
	}()

	wait := 60 * time.Second
	ws.SetReadDeadline(time.Now().Add(wait))
	for {
		var b [1500]byte
		l, err := ws.Read(b[:])
		if err != nil {
			glog.Errorf("error in websocket message read: %s", err)
			return
		}
		if l > 0 { // not a keepalive
			udpSync.Lock()
			if _, err := udpConn.WriteToUDP(b[:l], udpDestAddr); err != nil {
				glog.Errorf("could not write to udp conn: %s", err)
				quit <- struct{}{}
				udpSync.Unlock()
				return
			}
			udpSync.Unlock()
		}
		select {
		case <-c:
			return
		default:
		}
	}
}

func main() {
	flag.Parse()

	var err error
	udpDestAddr, err = net.ResolveUDPAddr("udp", *udpDest)
	if err != nil {
		glog.Errorf("udp_addr flag not resolvable: %s", err)
		return
	}
	go udp()
	go web()
	<-quit
}
