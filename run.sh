set -e
go get -v badc0de.net/pkg/wsudp/cmd/wsudp
wsudp --logtostderr "$@"
